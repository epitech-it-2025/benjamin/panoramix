##
## EPITECH PROJECT, 2020
## Makefile
## File description:
## Makefile
##

SRC			= 		main.c				\
					error.c				\
					panoramix.c			\
					druid.c				\
					villagers.c			\

CC 			= 		gcc

CPPFLAGS 	= 		-I./include/ -lpthread -Wall -Wextra

NAME		= 		panoramix

RM			= 		rm -rf

all:		$(NAME)

$(NAME):
			$(CC) -o $(NAME) $(addprefix src/, $(SRC)) $(CPPFLAGS)

debug:
		$(CC) -o $(NAME) $(addprefix src/, $(SRC)) $(CPPFLAGS) -g

prod:
	$(CC) -o $(NAME) $(addprefix src/, $(SRC)) $(CPPFLAGS) -Werror

clean:
			$(RM) $(NAME)

fclean: 	clean
			$(RM) $(NAME)
			find . -type f -name "*.gcda" -delete
			find . -type f -name "*.gcno" -delete
			find . -type f -name "*.o" -delete

re: 		fclean all

tests_run:	all

.PHONY: all clean fclean re lib lib_fclean tests_run debug prod
