#!/bin/bash

make; makeReturn=$?

if [ $makeReturn -eq 0 ] ; then
    echo -e "\E[32m Compilation OK ! \E[0m"
    exit 0
else    
    echo -e "\E[31m Erreur lors de la compilation \E[0m"
    exit 84

fi
