#!/bin/bash
apt-get update -y 
apt-get install cppcheck -y

cppcheck --error-exitcode=1 src/

ret_code=$?

if [ ret_code = "1" ] ; then
    echo -e "\E[31m Erreur  \E[0m"
    exit 84
else
    echo -e "\E[32m OK \E[0m"
    exit 0
fi

