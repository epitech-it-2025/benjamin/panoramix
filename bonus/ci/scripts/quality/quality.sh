#!/bin/bash

cp bonus/ci/scripts/quality/quality.rb src/

result=$(ruby src/quality.rb -i)

if [ -z "$result" ] ; then
    echo -e "\E[32m OK ! \E[0m"
    exit 0
else
    echo "$result"
    echo -e "\E[31m Erreur \E[0m"
    exit 0
fi

