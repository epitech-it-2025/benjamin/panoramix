/*
** EPITECH PROJECT, 2022
** panoramix [SSH: mouginet.fr]
** File description:
** panoramix
*/

#ifndef PANORAMIX_H_
    #define PANORAMIX_H_
    #define ARGS_MAX 5
    #include <pthread.h>
    #include <stdint.h>
    #include <semaphore.h>
    #include <stdio.h>

int args_error(int const ac, char const * const *av);
int panoramix(char const * const *av);
void *villagers(void *i);
void *druid(void* null);

typedef struct data_s {
    int nb_villagers;
    int pot_size;
    int nb_fight;
    int nb_refield;
    int default_pot_size;
    int battle_end;
}data_t;

extern data_t *data;
extern pthread_mutex_t refield;
extern sem_t pot_size;
extern sem_t semaphore;

#endif /* !PANORAMIX_H_ */
