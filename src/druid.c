/*
** EPITECH PROJECT, 2022
** panoramix [SSH: mouginet.fr]
** File description:
** druid
*/


#include "panoramix.h"
sem_t semaphore;

static void make_pot(void)
{
    if (data->battle_end == 0) {
        return;
    }
    printf("Druid: Ah! Yes, yes, I’m awake! Working on it! Beware "
    "I can only make %d more refills after this one.\n", data->nb_refield);
    if (data->nb_refield == 0) {
        printf("Druid: I’m out of viscum. I’m going back to... zZz\n");
    }
    data->pot_size = data->default_pot_size;
    sem_post(&pot_size);
}

void *druid(void *null)
{
    (void) null;

    printf("Druid: I’m ready... but sleepy...\n");
    while (data->nb_refield >= 0) {
        sem_wait(&semaphore);
        data->nb_refield--;
        make_pot();
    }
    return NULL;
}
