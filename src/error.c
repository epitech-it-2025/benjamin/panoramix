/*
** EPITECH PROJECT, 2022
** panoramix [SSH: mouginet.fr]
** File description:
** error
*/

#include <ctype.h>
#include <stdlib.h>
#include "panoramix.h"

static int is_only_num(char const *str)
{
    for (int i = 0; str[i] != '\0'; i++) {
        if (isdigit(str[i]) == 0)
            return 0;
    }
    return 1;
}

int args_error(int const ac, char const * const *av)
{
    if (ac < ARGS_MAX) {
        fprintf(stderr, "USAGE: ./panoramix <nb_villagers> <pot_size> "
        "<nb_fights> <nb_refills>\nValues must be >0.\n");
        return (84);
    }
    for (int i = 1; i < ac; i++) {
        if (is_only_num(av[i]) == 0) {
            fprintf(stderr, "USAGE: ./panoramix <nb_villagers> <pot_size> "
            "<nb_fights> <nb_refills>\nValues must be >0.\n");
            return 84;
        }
    }
    for (int i = 1; i < ac ; i++) {
        if (atoi(av[i]) <= 0) {
            fprintf(stderr, "USAGE: ./panoramix <nb_villagers> <pot_size> "
            "<nb_fights> <nb_refills>\nValues must be >0.\n");
            return 84;
        }
    }
    return 0;
}
