/*
** EPITECH PROJECT, 2021
** B-CPE-200-BDX-2-1-bsmatchstick-benjamin-maurel-mouginet
** File description:
** main
*/

#include "panoramix.h"

int main(int const ac, char const * const *av)
{
    if (args_error(ac, av) == 84) {
        return 84;
    }
    return (panoramix(av));
}
