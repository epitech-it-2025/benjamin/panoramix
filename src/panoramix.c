/*
** EPITECH PROJECT, 2022
** panoramix [SSH: mouginet.fr]
** File description:
** panoramix
*/

#include "panoramix.h"
#include <stdio.h>
#include <stdlib.h>

data_t *data;
pthread_mutex_t refield = PTHREAD_MUTEX_INITIALIZER;
int test = 3;
sem_t pot_size;

static void init_struct(char const * const *av)
{
    data = malloc(sizeof(data_t));
    if (data == NULL) {
        fprintf(stderr, "Error with data malloc \n");
        exit(84);
    }
    data->nb_villagers = atoi(av[1]);
    data->pot_size = atoi(av[2]);
    data->nb_fight = atoi(av[3]);
    data->nb_refield = atoi(av[4]);
    data->default_pot_size = atoi(av[2]);
    data->battle_end = data->nb_villagers;
}

static int init_all_villagers_thread(pthread_t *villager, data_t *data)
{
    for (int i = 0; i < data->nb_villagers; i++) {

        if ((pthread_create(&villager[i], NULL, villagers, (void*)i) != 0)) {
            perror("pthread_create failed\n");
            return 84;
        };
    }
    return 0;
}

int panoramix(char const * const *av)
{
    init_struct(av);
    pthread_t villagers[data->nb_villagers];
    pthread_t druid_struct[1];
    sem_init(&pot_size, 0, 0);

    pthread_create(druid_struct, NULL, druid, NULL);
    init_all_villagers_thread(villagers, data);
    for (int i = 0; i < data->nb_villagers; i++) {
        pthread_join(villagers[i], NULL);
    }
    pthread_cancel(druid_struct[0]);
    if (data->battle_end == 0) {
        sem_destroy(&pot_size);
        pthread_mutex_destroy(&refield);
    }
    return 0;
}
