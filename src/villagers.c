/*
** EPITECH PROJECT, 2022
** panoramix [SSH: mouginet.fr]
** File description:
** villagers
*/


#include "panoramix.h"

static void villagers_battle(int id, int battle_left)
{
    printf("Villager %d: Take that roman scum! Only %d left.\n",
    id, battle_left);
}

static int villager_drink(int id)
{
    if (data->pot_size > 0) {
        printf("Villager %d: I need a drink... I see %d serving left.\n",
        id, data->pot_size);
        data->pot_size--;
    } else {
        printf("Villager %d: I need a drink... I see %d serving left.\n",
        id, data->pot_size);
        if (data->nb_refield == 0) {
            return 1;
        }
        printf("Villager %d: Hey Pano wake up! We need more potion.\n", id);
        sem_post(&semaphore);
        sem_wait(&pot_size);
        data->pot_size--;
    }
    return 0;
}

static int loop(int id)
{
    int i = data->nb_fight -1;

    for (; i >= 0; i--) {
        if (data->nb_refield == 0 && data->pot_size == 0) {
            return 1;
        }
        pthread_mutex_lock(&refield);
        if (villager_drink(id) == 1) {
            pthread_mutex_unlock(&refield);
            return 1;
        }
        villagers_battle(id, i);
        pthread_mutex_unlock(&refield);
    }
    return 0;
}

void *villagers(void *thread_id)
{
    int id = (intptr_t)thread_id;

    printf("Villager %d: Going into battle!\n", id);
    if (loop(id) == 1) {
        return NULL;
    }
    printf("Villager %d: I 'm going to sleep now.\n", id);
    data->battle_end--;
    if (data->battle_end == 0) {
        while (data->nb_refield >= 0) {
            sem_post(&semaphore);
        }
        sem_close(&semaphore);
    }
    return NULL;
}
