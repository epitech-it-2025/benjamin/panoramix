/*
** EPITECH PROJECT, 2020
** mini-shell1
** File description:
** tests_my_env.c
*/

#include <criterion/criterion.h>
#include <criterion/redirect.h>

void redirect_all_stdout(void)
{
    cr_redirect_stdout();
    cr_redirect_stderr();
}

Test(my_env, basic_print, .init=redirect_all_stdout)
{

}